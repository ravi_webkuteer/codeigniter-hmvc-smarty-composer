{extends file = 'default.tpl'}

{block name="content"}
<h1>{$this->lang->language.homepage_title} {$name}</h1>

<div id="body">
<pre>
CodeIgniter 2.1.4 + Smarty 3 + Composer & HMVC Support
-------------------------------------------------------------------------------

CodeIgniter is a proven, agile & open PHP web application framework, with a
small footprint, that's powering the next generation of web apps.

However, the framework's standard template parsing engine needs the use of PHP
tags in order to function and that's not very good for the designer's teams as 
a much more powerful template engine is needed.

That's where the Smarty template engine picks up. With simple tags, you can 
output the info you need and with it's powerful extensible template files, you 
don't need to redo all of your work in each view you create.
- More info on Smarty, here: http://www.smarty.net/documentation

But still, CodeIgniter, with it's powerful MVC approach, would make larger 
projects became hard to maintain as a team and also kept the code you created 
hard to reuse in your next project. For example, using your user authentication 
model class in different projects, and all your views, controllers and hooks, 
was a very painful situation. That's why I've assembled the HMVC support and 
tweaked it to also auto-load your hooks. That way, you can keep your work as 
plug-n-play modules and reuse it as needed without much effort.

Last, but not least, Composer. This package and dependencies manager is the best 
thing for PHP projects to keep a light distribution package size, and keep all 
the dependencies versions in check, reducing the problems of assemble them in 
the installer phase of the project. By default, this fork of the CodeIgniter 
project does not have the CodeIgniter's System folder, as it install it though 
composer package. See https://bitbucket.org/josepostiga/codeigniter-system 
for more info.

Please, feel free to fork it, modify it and talk to me about what can be done to 
keep this repository up-to-date and interesting.
</pre>
</div> 
{/block}